[TOC]

# Release 1.2.0 - 2023-08-23

* Add experimental objective functions for NxTD
* Add optional joining of variables for NxTD
* Fix file name issue for TDLib
* Reformat code

# Release 1.1.0 - 2021-12-03

Reformat / code style

## Bug Fix
	- expand root node
